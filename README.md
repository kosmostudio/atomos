# Atomos

A component library built with Storybook, React, Typescript and Webpack 4.

* HMR for development
* JavaScript compiles down to ES5

## Instructions

__Running storybook__

1. Install dependencies: `npm install`
2. Run storybook: `npm run storybook`

__Creating library__

1. Install dependencies: `npm install`
2. Compile code: `npm run compile`
3. Create Typescript declarations `npm run tsd`

## Licence

Licensed under [MIT](https://opensource.org/licenses/MIT) licence.