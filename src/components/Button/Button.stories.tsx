import React from 'react'

import { Button, ButtonProps } from './Button'

export default {
  component: Button,
  title: 'Components/Button',
}

const Template = (args: ButtonProps) => <Button {...args} />

export const Default = Template.bind({})

Default.args = {
  text: 'Download',
  loading: false,
  disabled: false,
}
