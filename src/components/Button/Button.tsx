import React, { useRef, useEffect, useState } from 'react'
import { Loader } from '../Loader/Loader'
import { buttonWidth } from '../../helpers/buttonWidth'

export type ButtonProps = {
  text: string
  onClick: () => void
  disabled?: boolean
  loading?: boolean
}

export const Button: React.FC<ButtonProps> = ({
  text,
  onClick,
  disabled,
  loading,
}) => {
  const [width, setWidth] = useState(0)
  const button = useRef()
  useEffect(() => {
    if (!button) return
    const { current } = button
    if (!current) return
    const width = buttonWidth(current)
    setWidth(width)
  }, [])
  return (
    <button
      ref={button}
      className="button"
      onClick={!disabled ? onClick : null}
      disabled={disabled || loading ? true : false}
      style={{
        textIndent: loading ? '1.6rem' : 0,
        maxWidth: width !== 0 ? width + 1 : '100%',
      }}
    >
      {text}
      {loading ? <Loader /> : null}
    </button>
  )
}
