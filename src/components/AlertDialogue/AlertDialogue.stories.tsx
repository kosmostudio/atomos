import React from 'react'

import { AlertDialogue, AlertDialogueProps } from './AlertDialogue'

export default {
  title: 'Components/AlertDialogue',
  component: AlertDialogue,
  argTypes: {
    type: {
      control: {
        type: 'select',
      },
      options: ['failure', 'warning', 'success'],
    },
  },
}

const Template = (args: AlertDialogueProps) => (
  <AlertDialogue {...args}>Bad response from server</AlertDialogue>
)

export const Default = Template.bind({})

Default.args = {
  type: 'success',
}
