import React, { ReactNode, useEffect, Dispatch, SetStateAction } from 'react'

export type AlertDialogueProps = {
  type?: 'warning' | 'failure' | 'success'
  setAlert: Dispatch<SetStateAction<Alert>>
  children: ReactNode
}

type Alert = {
  hasAlert?: boolean
  AlertInfo?: string
}

export const AlertDialogue: React.FC<AlertDialogueProps> = ({
  type,
  children,
  setAlert,
}) => {
  useEffect(() => {
    setTimeout(() => setAlert({ hasAlert: false, AlertInfo: undefined }), 5000)
  }, [])
  return (
    <div className={`alertDialogue ${type}`}>
      <p>{children}</p>
    </div>
  )
}
