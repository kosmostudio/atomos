import React from 'react'

import { InputSubmit, InputSubmitProps } from './InputSubmit'

export default {
  component: InputSubmit,
  title: 'Components/InputSubmitButton',
}

const Template = (args: InputSubmitProps) => <InputSubmit {...args} />

export const Default = Template.bind({})

Default.args = {
  text: 'Submit',
}
