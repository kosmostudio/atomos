import React from 'react'

export interface InputSubmitProps {
  text: string
}

export const InputSubmit: React.FC<InputSubmitProps> = ({text}) => {
  return (
    <input type="submit" value={text}/>
  )
}