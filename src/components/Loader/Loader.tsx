import React from 'react'
import styles from './loader.module.css'

export type LoaderProps = {
  size?: string
}

export const Loader: React.FC<LoaderProps> = ({ size }) => {
  return (
    <div
      className={`${styles.loader} ${
        size === 'large' ? styles.loaderLarge : styles.loaderSmall
      }`}
    ></div>
  )
}
