import React from 'react'

import { Loader, LoaderProps } from './Loader'

export default {
  title: 'Components/Loader',
  component: Loader,
}

const Template = (args: LoaderProps) => <Loader {...args} />

export const Default = Template.bind({})

Default.args = {
  size: 'large',
}
