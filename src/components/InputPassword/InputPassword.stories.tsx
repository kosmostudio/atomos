import React from 'react'

import { InputPassword, InputPasswordProps } from './InputPassword'

export default {
  component: InputPassword,
  title: 'Components/InputPassword',
}

const Template = (args: InputPasswordProps) => <InputPassword {...args} />

export const Default = Template.bind({})

Default.args = {
  text: '',
  placeholder: 'Password',
  required: true,
}
