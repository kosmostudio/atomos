import React, { ChangeEvent, useState } from 'react'
import { validatePassword } from '../../helpers/validatePassword'

export interface InputPasswordProps {
  text: string
  placeholder: string
  required: boolean
  onChange: (e: ChangeEvent<HTMLInputElement>) => void
}

export const InputPassword: React.FC<InputPasswordProps> = ({
  text,
  placeholder,
  required,
  onChange,
}) => {
  const [error, setError] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')
  const [focus, setFocus] = useState(false)

  const handleBlur = (): void => {
    setFocus(!focus)

    if (!required) {
      return
    }

    const passwordError = validatePassword(text)

    if (passwordError.length === 0) {
      setError(false)
      return
    }

    setErrorMessage(passwordError)
    setError(true)
  }

  return (
    <div className={required ? 'required' : 'not-required'}>
      <label className={focus ? 'label' : ''}>{placeholder}</label>
      <span>{error ? errorMessage : ''}</span>
      <input
        className={error ? 'input-error' : ''}
        type="password"
        value={text}
        placeholder={focus ? '' : placeholder}
        required={required}
        onChange={onChange}
        onBlur={handleBlur}
        onFocus={() => setFocus(!focus)}
      />
    </div>
  )
}
