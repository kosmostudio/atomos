import React, {useState, useRef, ChangeEvent, useEffect} from 'react'

export interface VideoPlayerProps {
  videoUrl: string
  videoType: string
  videoPoster: string
}

export const VideoPlayer: React.FC<VideoPlayerProps> = ({videoUrl, videoType, videoPoster}) => {

  const videoEl = useRef(null)
  const [playStatus, setPlayStatus] = useState(false)
  const [muteStatus, setMuteStatus] = useState(false)
  const [seekBar, setSeekBar] = useState(0)
  const [volume, setVolume] = useState('.7')

  const handlePlayClick = () => {
    if (videoEl.current.paused === true ) {
      videoEl.current.play()
      setPlayStatus(true)
    } else {
      videoEl.current.pause()
      setPlayStatus(false)
    }
  }

  const handleMuteClick = () => {
    if (videoEl.current.muted === false) {
      videoEl.current.muted = true
      setMuteStatus(true)
    } else {
      videoEl.current.muted = false
      setMuteStatus(false)
    }
  }

  const handleFullScreenClick = () => {
    if (videoEl.current.requestFullscreen) {
      videoEl.current.requestFullscreen()
    } else if (videoEl.current.mozRequestFullscreen) {
      videoEl.current.mozRequestFullscreen()
    } else if (videoEl.current.webkitRequestFullscreen) {
      videoEl.current.webkitRequestFullScreen()
    } else if (videoEl.current.msRequestFullscreen) {
      videoEl.current.msRequestFullscreen()
    }
  }

  const handleSeekBarChange = (e: ChangeEvent<HTMLInputElement>) => {
    setSeekBar(parseInt(e.target.value))
    const time = videoEl.current.duration * (seekBar / 100)
    videoEl.current.currentTime = time
  }

  const handleVolumeChange = (e: ChangeEvent<HTMLInputElement>) => {
    setVolume(e.target.value)
    videoEl.current.volume = e.target.value
  }

  const updateSeekBar = () => {
    window.setInterval( (t: number) => {
      if (videoEl.current.readyState > 0) {
        const value = (100 / videoEl.current.duration) * videoEl.current.currentTime
        setSeekBar(value)
      }
      clearInterval(t)
    }, 500)
  }

  useEffect( () => {
    window.addEventListener('timeupdate', updateSeekBar)
    return () => {
      window.removeEventListener('timeupdate', updateSeekBar)
    }}
  )

  return (
    <>
      <video ref={videoEl} poster={videoPoster}>
        <source src={videoUrl} type={`video/${videoType}`}/>
      </video>
      <div className="video-controls">
        <div className={playStatus ? 'pause' : 'play'} onClick={handlePlayClick}><span></span></div>
        <input className="seek" type="range" onChange={handleSeekBarChange} value={seekBar}/>
        <div className={muteStatus ? 'volume' : 'mute'} onClick={handleMuteClick}><span></span></div>
        <input className="volume" type="range" min={0} max={1} step={0.1} onChange={handleVolumeChange} value={volume} />
        <div className="full-screen" onClick={handleFullScreenClick}><span></span></div>
      </div>
    </>
  )
}