import React from 'react'

import { VideoPlayer, VideoPlayerProps } from './VideoPlayer'

export default {
  component: VideoPlayer,
  title: 'Components/VideoPlayer',
}

const Template = (args: VideoPlayerProps) => <VideoPlayer {...args} />

export const Default = Template.bind({})

Default.args = {
  videoUrl:
    'https://s3-ap-southeast-2.amazonaws.com/artgrounds.art/videos/paper-making.mp4',
  videoType: 'mp4',
  videoPoster:
    'https://s3-ap-southeast-2.amazonaws.com/artgrounds.art/videos/paper-making.jpg',
}
