import React, { ChangeEvent, useState } from 'react'
import { validateCode } from '../../helpers/validateCode'

export interface InputConfirmationCodeProps {
  code: string
  placeholder: string
  required: boolean
  onChange: (e: ChangeEvent<HTMLInputElement>) => void
}

export const InputConfirmationCode: React.FC<InputConfirmationCodeProps> = ({
  code,
  placeholder,
  required,
  onChange,
}) => {
  const [error, setError] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')
  const [focus, setFocus] = useState(false)

  const handleBlur = (): void => {
    setFocus(!focus)

    if (!required) {
      return
    }

    const codeError = validateCode(code, placeholder)

    if (codeError.length === 0) {
      setError(false)
      return
    }

    setErrorMessage(codeError)
    setError(true)
  }

  return (
    <div className={required ? 'required' : 'not-required'}>
      <label className={focus ? 'label' : ''}>{placeholder}</label>
      <span>{error ? errorMessage : ''}</span>
      <input
        className={error ? 'input-error' : ''}
        type="text"
        value={code}
        placeholder={focus ? '' : placeholder}
        required={required}
        onChange={onChange}
        onBlur={handleBlur}
        onFocus={() => setFocus(!focus)}
      />
    </div>
  )
}
