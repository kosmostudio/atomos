import React from 'react'

import {
  InputConfirmationCode,
  InputConfirmationCodeProps,
} from './InputConfirmationCode'

export default {
  component: InputConfirmationCode,
  title: 'Components/InputConfirmationCode',
}

const Template = (args: InputConfirmationCodeProps) => (
  <InputConfirmationCode {...args} />
)

export const Default = Template.bind({})

Default.args = {
  code: '',
  placeholder: 'Confirmation code',
  required: true,
}
