import React from 'react'

import { Modal, ModalProps } from './Modal'

export default {
  title: 'Components/Modal',
  component: Modal,
}

const Template = (args: ModalProps) => (
  <Modal {...args}>
    <>
      <p>
        <strong>Example content</strong>
      </p>
      <ul>
        <li>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Error,
          suscipit repellendus. Ea ut dicta voluptas omnis sequi, veniam quia
          vel laboriosam, itaque et quos odit minus modi, consequuntur quisquam
          eligendi.
        </li>
        <li>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Error,
          suscipit repellendus. Ea ut dicta voluptas omnis sequi, veniam quia
          vel laboriosam, itaque et quos odit minus modi, consequuntur quisquam
          eligendi.
        </li>
      </ul>

      <p>
        <strong>More content</strong>
      </p>
      <ul>
        <li>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Error,
          suscipit repellendus. Ea ut dicta voluptas omnis sequi, veniam quia
          vel laboriosam, itaque et quos odit minus modi, consequuntur quisquam
          eligendi.
        </li>
        <li>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Error,
          suscipit repellendus. Ea ut dicta voluptas omnis sequi, veniam quia
          vel laboriosam, itaque et quos odit minus modi, consequuntur quisquam
          eligendi.
        </li>
        <li>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Error,
          suscipit repellendus. Ea ut dicta voluptas omnis sequi, veniam quia
          vel laboriosam, itaque et quos odit minus modi, consequuntur quisquam
          eligendi.
        </li>
        <li>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Error,
          suscipit repellendus. Ea ut dicta voluptas omnis sequi, veniam quia
          vel laboriosam, itaque et quos odit minus modi, consequuntur quisquam
          eligendi.
        </li>
      </ul>
    </>
  </Modal>
)

export const Default = Template.bind({})
