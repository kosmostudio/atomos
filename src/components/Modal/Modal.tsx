import React, { ReactNode, Dispatch, SetStateAction } from 'react'
import CloseIcon from '../../icons/close.svg'

export type ModalProps = {
  setModal: Dispatch<SetStateAction<boolean>>
  children: ReactNode
}

export const Modal: React.FC<ModalProps> = ({ setModal, children }) => {
  return (
    <div className={`${'modal'} `}>
      <div className="modalContainer">
        <CloseIcon
          className="closeIcon"
          onClick={() => setModal(false)}
          width="36px"
        />

        <div className="modalContent">{children}</div>
      </div>
    </div>
  )
}
