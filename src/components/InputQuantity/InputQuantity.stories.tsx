import React from 'react'

import { InputQuantity, InputQuantityProps } from './InputQuantity'

export default {
  component: InputQuantity,
  title: 'Components/InputQuantity',
}

const Template = (args: InputQuantityProps) => <InputQuantity {...args} />

export const Default = Template.bind({})

Default.args = {
  quantity: 1,
  min: 1,
  max: 25,
}
