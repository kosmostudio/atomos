import React, { ChangeEvent, useRef } from 'react'

export interface InputQuantityProps {
  id: string
  quantity: number
  max: number
  min: number
  onChange: (e: ChangeEvent<HTMLInputElement>) => void
  onClick: (value: number, id: string) => void
}

export const InputQuantity: React.FC<InputQuantityProps> = ({
  id,
  quantity,
  max,
  min,
  onChange,
  onClick,
}) => {
  const inputEl = useRef(null)

  const increment = (): void => {
    const id = inputEl.current.id
    if (quantity === max) {
      return
    }
    onClick(quantity + 1, id)
  }

  const decrement = (): void => {
    const id = inputEl.current.id
    if (quantity === min) {
      return
    }
    onClick(quantity - 1, id)
  }

  return (
    <div className="quantity">
      <input
        ref={inputEl}
        id={id}
        type="number"
        value={quantity}
        max={max}
        min={min}
        onChange={onChange}
      />
      <span className="increment" onClick={increment}></span>
      <span className="decrement" onClick={decrement}></span>
    </div>
  )
}
