import React from 'react'

import { SelectCountry, SelectCountryProps } from './SelectCountry'

export default {
  component: SelectCountry,
  title: 'Components/SelectCountry',
}

const Template = (args: SelectCountryProps) => <SelectCountry {...args} />

export const Default = Template.bind({})

Default.args = {
  text: '',
  placeholder: 'Select country',
  required: false,
}
