import React, { ChangeEvent, useState } from 'react'
import { countries } from './countries'
import { validateCountry } from '../../helpers/validateCountry'

export interface SelectCountryProps {
  text: string
  placeholder: string
  required: boolean
  selectChange: (e: ChangeEvent<HTMLSelectElement>) => void
}

export const SelectCountry: React.FC<SelectCountryProps> = ({
  text,
  placeholder,
  required,
  selectChange,
}) => {
  const [error, setError] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')
  const [focus, setFocus] = useState(false)

  const handleBlur = (): void => {
    setFocus(!focus)

    if (!required) {
      return
    }

    const selectError = validateCountry(text)

    if (selectError.length === 0) {
      setError(false)
      return
    }

    setErrorMessage(selectError)
    setError(true)
  }

  return (
    <div className={required ? 'required' : 'not-required'}>
      <label className={focus ? 'label' : ''}>{placeholder}</label>
      <span>{error ? errorMessage : ''}</span>
      <select
        name="countries"
        onChange={selectChange}
        value={text}
        onBlur={handleBlur}
        onFocus={() => setFocus(!focus)}
        required={required}
      >
        <option value="">{placeholder}</option>
        {countries.map((country, i) => {
          return (
            <option key={`country-${i}`} value={country}>
              {country}
            </option>
          )
        })}
      </select>
    </div>
  )
}
