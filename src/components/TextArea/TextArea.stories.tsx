import React from 'react'

import { TextArea, TextAreaProps } from './TextArea'

export default {
  component: TextArea,
  title: 'Components/TextArea',
}

const Template = (args: TextAreaProps) => <TextArea {...args} />

export const Default = Template.bind({})

Default.args = {
  placeholder: 'Description',
}
