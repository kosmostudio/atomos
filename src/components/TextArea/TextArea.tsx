import React, { ChangeEvent, useState } from 'react'

export interface TextAreaProps {
  text: string
  placeholder: string
  onChange: (e: ChangeEvent<HTMLTextAreaElement>) => void
}

export const TextArea: React.FC<TextAreaProps> = ({
  text,
  placeholder,
  onChange,
}) => {
  const [focus, setFocus] = useState(false)

  return (
    <div className="not-required">
      <label className={focus ? 'label' : ''}>{placeholder}</label>
      <textarea
        value={text}
        placeholder={focus ? '' : placeholder}
        onChange={onChange}
        cols={40}
        rows={15}
        onFocus={() => setFocus(!focus)}
        onBlur={() => setFocus(!focus)}
      />
    </div>
  )
}
