import React, { ChangeEvent, useState } from 'react'
import { validateEmail } from '../../helpers/validateEmail'

export interface InputEmailProps {
  text: string
  placeholder: string
  required: boolean
  onChange: (e: ChangeEvent<HTMLInputElement>) => void
}

export const InputEmail: React.FC<InputEmailProps> = ({
  text,
  placeholder,
  required,
  onChange,
}) => {
  const [error, setError] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')
  const [focus, setFocus] = useState(false)

  const handleBlur = (): void => {
    setFocus(!focus)

    if (!required) {
      return
    }

    const emailError = validateEmail(text)

    if (emailError.length === 0) {
      setError(false)
      return
    }

    setErrorMessage(emailError)
    setError(true)
  }

  return (
    <div className={required ? 'required' : 'not-required'}>
      <label className={focus ? 'label' : ''}>{placeholder}</label>
      <span>{error ? errorMessage : ''}</span>
      <input
        className={error ? 'input-error' : ''}
        type="email"
        value={text}
        placeholder={focus ? '' : placeholder}
        required={required}
        onChange={onChange}
        onBlur={handleBlur}
        onFocus={() => setFocus(!focus)}
      />
    </div>
  )
}
