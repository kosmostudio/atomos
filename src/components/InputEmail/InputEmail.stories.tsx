import React from 'react'

import { InputEmail, InputEmailProps } from './InputEmail'

export default {
  component: InputEmail,
  title: 'Components/InputEmail',
}

const Template = (args: InputEmailProps) => <InputEmail {...args} />

export const Default = Template.bind({})

Default.args = {
  text: '',
  placeholder: 'Email',
  required: true,
}
