import React from 'react'

import { InputText, InputTextProps } from './InputText'

export default {
  component: InputText,
  title: 'Components/InputText',
}

const Template = (args: InputTextProps) => <InputText {...args} />

export const Default = Template.bind({})

Default.args = {
  text: '',
  placeholder: 'Year',
  required: false,
}
