import React, { ChangeEvent, useState } from 'react'
import { validateText } from '../../helpers/validateText'

export interface InputTextProps {
  text: string
  placeholder: string
  required?: boolean
  onChange: (e: ChangeEvent<HTMLInputElement>) => void
}

export const InputText: React.FC<InputTextProps> = ({
  text,
  placeholder,
  required,
  onChange,
}) => {
  const [error, setError] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')
  const [focus, setFocus] = useState(false)

  const handleBlur = (): void => {
    setFocus(!focus)

    if (!required) {
      return
    }

    const textError = validateText(text, placeholder)

    if (textError.length === 0) {
      setError(false)
      return
    }

    setErrorMessage(textError)
    setError(true)
  }

  return (
    <div className={required ? 'required' : 'not-required'}>
      <label className={focus ? 'label' : ''}>{placeholder}</label>
      <span>{error ? errorMessage : ''}</span>
      <input
        className={error ? 'input-error' : ''}
        type="text"
        value={text}
        placeholder={focus ? '' : placeholder}
        required={required}
        onChange={onChange}
        onBlur={handleBlur}
        onFocus={() => setFocus(!focus)}
      />
    </div>
  )
}
