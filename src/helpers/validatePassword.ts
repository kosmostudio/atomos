export function validatePassword(password: string): string {
  let passwordError = ''
  if (password.length === 0) {
    passwordError = 'Password can\'t be empty'
  } else if (password.length < 4) {
    passwordError = 'Password should be at least 4 characters long'
  } else if ( !(password.match(/[a-z]/g)) ) {
    passwordError = 'Password should contain a lower case letter'
  } else if ( !(password.match(/[A-Z]/g)) ) {
    passwordError = 'Password should contain a capital letter'
  } else if ( !(password.match(/[0-9]/g)) ) {
    passwordError = 'Password should contain a number'
  } else if ( !(password.match(/[-!$%^&*()_+|~=`{}[:;<>?,.@#\]]/g)) ) {
    passwordError = 'Password should contain a special character'
  }
  return passwordError
}