export function validateCode(code: string, placeholder: string): string {
  if(code.length === 0) {
    return `${placeholder} can\'t be empty`
  } else if (code.length !== 6) {
    return `${placeholder} must be made up of 6 numbers`
  }
  return ''
}