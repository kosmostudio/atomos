export function validateText(text: string, placeholder: string): string {
  if(text.length === 0) {
    return `${placeholder} can\'t be empty`
  }
  return ''
}