export function validateEmail(email: string): string {
  let emailError = ''
  if (email.length === 0) {
    emailError = 'Email can\'t be empty'
  } else if (email.length < 5) {
    emailError = 'Email should be at least 5 characters long'
  } else if (email.split('').filter(x => x === '@').length !== 1) {
    emailError = 'Email should contain a @'
  } else if (email.indexOf('.') === -1) {
    emailError = 'Email should contain at least one dot'
  }
  return emailError
}