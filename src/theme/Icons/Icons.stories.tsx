import React from 'react'
import styles from './icons.module.css'
import AddIcon from '../../icons/add.svg'
import AsteriskIcon from '../../icons/asterisk.svg'
import ChevronIcon from '../../icons/chevron.svg'
import FullScreenIcon from '../../icons/full-screen.svg'
import MuteIcon from '../../icons/mute.svg'
import PauseIcon from '../../icons/pause.svg'
import PlayIcon from '../../icons/play.svg'
import RemoveIcon from '../../icons/remove.svg'
import VolumeIcon from '../../icons/volume.svg'
import EditIcon from '../../icons/edit.svg'
import CancelIcon from '../../icons/cancel.svg'
import CloseIcon from '../../icons/close.svg'
import CheckedBoxIcon from '../../icons/checked-box.svg'
import UncheckedBoxIcon from '../../icons/unchecked-box.svg'
import StarIcon from '../../icons/star.svg'
import StarFilledIcon from '../../icons/star-filled.svg'
import MenuIcon from '../../icons/menu.svg'

export default {
  title: 'Theme/Icons',
}

export const Default: React.FC = () => {
  return (
    <>
      <h2>Default Icons</h2>
      <div className={styles.iconGroup}>
        <AddIcon />
        <AsteriskIcon />
        <ChevronIcon />
        <FullScreenIcon />
        <MuteIcon />
        <PauseIcon />
        <PlayIcon />
        <RemoveIcon />
        <VolumeIcon />
        <EditIcon />
        <CancelIcon />
        <CloseIcon />
        <UncheckedBoxIcon />
        <CheckedBoxIcon />
        <StarIcon />
        <StarFilledIcon />
        <MenuIcon />
      </div>
    </>
  )
}
