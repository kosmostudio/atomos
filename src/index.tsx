import * as styles from './css/styles.css'
export { styles }

export * from './components/Button/Button'
export * from './components/InputSubmit/InputSubmit'
export * from './components/InputText/InputText'
export * from './components/InputEmail/InputEmail'
export * from './components/InputPassword/InputPassword'
export * from './components/TextArea/TextArea'
export * from './components/InputQuantity/InputQuantity'
export * from './components/InputConfirmationCode/InputConfirmationCode'
export * from './components/SelectCountry/SelectCountry'
export * from './components/VideoPlayer/VideoPlayer'
export * from './components/Modal/Modal'
export * from './components/AlertDialogue/AlertDialogue'

// Icons
export { default as AddIcon } from './icons/add.svg'
export { default as AsteriskIcon } from './icons/asterisk.svg'
export { default as CancelIcon } from './icons/cancel.svg'
export { default as ChevronIcon } from './icons/chevron.svg'
export { default as CloseIcon } from './icons/close.svg'
export { default as EditIcon } from './icons/edit.svg'
export { default as FullScreenIcon } from './icons/full-screen.svg'
export { default as MuteIcon } from './icons/mute.svg'
export { default as PauseIcon } from './icons/pause.svg'
export { default as PlayIcon } from './icons/play.svg'
export { default as RemoveIcon } from './icons/remove.svg'
export { default as VolumeIcon } from './icons/volume.svg'
export { default as CheckedBoxIcon } from './icons/checked-box.svg'
export { default as UncheckedBoxIcon } from './icons/unchecked-box.svg'
export { default as StarIcon } from './icons/star.svg'
export { default as StarFilledIcon } from './icons/star-filled.svg'
export { default as MenuIcon } from './icons/menu.svg'
