const custom = require('../webpack.config')
module.exports = {
  stories: ['../src/**/*.stories.tsx'],

  webpackFinal: (config) => {
    config.resolve.extensions.push('.ts', '.tsx', '.js', '.css')
    return {
      ...config,
      module: {
        ...config.module,
        rules: custom.module.rules,
      },
    }
  },

  addons: ['@storybook/addon-essentials'],

  framework: {
    name: '@storybook/react-webpack5',
    options: {}
  },

  docs: {
    autodocs: true
  }
}
