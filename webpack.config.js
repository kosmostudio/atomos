const { resolve } = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const postcssPresetEnv = require('postcss-preset-env')

console.log('Node env: ', process.env.NODE_ENV)
const devMode = process.env.NODE_ENV === 'development'

module.exports = {
  entry: './src/index.tsx',
  output: {
    filename: 'index.js',
    path: resolve(__dirname, 'dist'),
    libraryTarget: 'umd',
    globalObject: 'this',
  },
  mode: 'production',
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.css'],
  },
  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        exclude: /node_modules/,
        use: 'babel-loader',
      },
      {
        test: /\.css$/,
        use: [
          devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
          'css-modules-typescript-loader',
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              sourceMap: true,
              modules: {
                auto: true,
                localIdentName: '[name]__[local]___[hash:base64:5]',
              },
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [postcssPresetEnv({ stage: 0 })],
              },
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        issuer: /\.ts(x?)$/,
        use: [
          { loader: 'babel-loader' },
          {
            loader: '@svgr/webpack',
            options: {
              ext: 'tsx',
              typescript: true,
              babel: false,
            },
          },
        ],
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        issuer: /\.css$/,
        type: 'asset/resource',
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: 'css/styles.css',
      chunkFilename: 'css/[id].css',
    }),
  ],
  externals: {
    react: 'react',
    'react-dom': 'react-dom',
  },
}
